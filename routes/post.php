<?php

use \Illuminate\Http\Requset;
use \Illuminate\Support\Facades\Route;


// Route::get('/:title', function(){
//     dd('Return post single page');
// });


Route::post('/new', 'PostController@create');

Route::get('/{id}', 'PostController@view');

Route::post('/delete', 'PostController@delete');

Route::get('/edit/{id}', 'PostController@edit');

Route::post('/edit', 'PostController@update');



