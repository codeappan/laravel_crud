@extends('master')
@section('title', 'Edit Message')
    
@section('content')
    <div class="container">
        <div class="p-5">
            <a href="/" class="btn btn-outline-primary mb-3"><i class="fas fa-arrow-left"></i></a>
        <h3>Edit Message: {{ $message->title}}</h3>
        
            <form action="/post/edit" class="mt-4" method="POST">

                <div class="row">
                  <div class="col">
                    <input type="text" name="title" class="form-control" value="{{$message->title}}"  placeholder="Title">
                  </div>
                  <div class="col">
                    <input type="text" name="content" class="form-control" value="{{$message->content}}" placeholder="Content">
                  </div>
                  <div class="col">
                  <input type="hidden" name="id" value="{{$message->id}}">
                      {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Update</button>
                  </div>
                </div>
              </form>
        </div>
    </div>
@endsection