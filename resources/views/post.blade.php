@extends('master')

@section('title')
    {{ $message->title}}
@endsection 

@section('content')
    
    <div class="container p-5">
        <a href="/" class="btn btn-outline-primary mb-3"><i class="fas fa-arrow-left"></i></a>
        
        <h2> {{ $message->title}} </h2>
        <p> {{ $message->content}} </p>
        <small>{{$message->created_at->diffForHumans()}} @if($message->created_at != $message->updated_at) <span title="last updated on {{$message->updated_at->diffForHumans() }}">(edited)</span>   @endif  </small> 
    </div>
@endsection