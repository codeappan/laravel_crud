
@extends('master')
@section('title', 'Homepage')

@section('content')
    
    <div class="container">

        <div class="p-5">
          
           @if ($success)
           <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> {{$success}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            @endif
            <h4>New Post:</h4>

            @if ($error)
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              <strong>Oh no!</strong> {{$error}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            @endif 

           
         
            <form action="/post/new" class="mt-4" method="POST">

                <div class="row">
                  <div class="col-4">
                    <input type="text" name="title" class="form-control" placeholder="Title" required>
                  </div>
                  <div class="col-8">
                    <input type="text" name="content" class="form-control" placeholder="Content" required>
                  </div>
                  
                </div>
                <br>
                <div class="row">
                  <div class="col">
                    <input type="text" class="form-control" name="tags" placeholder="Tags">
                  </div>
                  <div class="col">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success">Create</button>
                  </div>
                </div>
              </form>
        </div>
        
        <div>

            @if ( count($messages) !== 0 )
            <h3>Recent Posts:</h3>
            @else
            <h3 class="text-center">No Posts!</h3>
            @endif
            

            <ul class="list-group list-group-flush">
           
                @foreach ($messages as $key => $message)
                {{-- <p>{{$message}}</p> --}}
                <li class="list-group-item"> 
                    <form action="/post/delete" method="post" class="float-right ml-2"> 
                        <input  type="hidden" name="id" value="{{$message['id']}}"> 
                        {{ csrf_field() }} 
                        <button class="btn btn-sm btn-outline-warning" type="submit"><i class="fas fa-trash-alt"></i></button>
                    </form>
                    <a href="/post/edit/{{$message['id']}}" class="float-right">
                        <button class="btn btn-sm btn-outline-success" type="submit"><i class="fas fa-edit"></i></button>
                    </a>
                    
                    <a href="/post/{{$message['id']}}"> {{ $message['title']}}</a> <br>
                    {{ $message['content'] }} <br>

                    @foreach ($message['tags'] as $koy=> $tag)

                    @php
                        $array = (object) $tag //convert to object
                    @endphp
                    {{-- <p>{{print_r($array)}}</p> --}}
                    {{-- {{ $value = (array) $tag }} --}}
                    {{-- {{ $array = json_decode(json_encode($tag), true) }}
                    <p>{{print_r($array->name)}}</p> --}}
                  {{-- <span>{{ print_r($tag)}}</span> --}}
                  {{-- <p>{{count($message['tags'])}}</p> --}}
                    <span><a href="/tagged/{{ $array->name }}">{{ $array->name }}</a>@if (count($message['tags']) !== $koy+1),@endif</span>
                    @endforeach
                  
                  {{-- <p>{{ Carbon\Carbon::parse($message->created_at)->diffForHumans() }}</p> --}}
                    <small>{{Carbon\Carbon::parse($message['created_at'])->diffForHumans()}} @if($message['created_at'] != $message['updated_at']) <span title="last updated on {{Carbon\Carbon::parse($message['updated_at'])->diffForHumans()}}">(edited)</span>   @endif  </small>
                    
                @endforeach
                
              </ul>

        
        </div>
        
    </div>
@endsection