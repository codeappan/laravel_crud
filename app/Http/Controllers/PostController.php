<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Posts;

class PostController extends Controller
{
    public function create(Request $request){


        $title = $request->title;
        $content = $request->content;
        $tags = \explode(",", $request->tags);


       

        //new table content
        // $message = new Message();
        // $message->title = $request->title;
        // $message->content = $request->content;
        // save to db
        // $message->save();

        try {
            $messageId = DB::table('posts')->insertGetId([
                "created_at"=>date('Y-m-d H:i:s'),
                "updated_at"=>date('Y-m-d H:i:s'),
                "title"=>$title,
                "content"=>$content
            ]);
        } catch (\Throwable $err) {
            return \redirect('/')->with("errMsg", $err);
        }
    
    $tagIds = array();
    
    foreach ($tags as $tag) {
        try {
            $tagId = DB::table('tags')->insertGetId([
                "name"=>$tag
                ]);
            \array_push($tagIds, $tagId);
        } catch (\Throwable $err) {
            $eTag = DB::table('tags')->where('name',$tag)->first();
            // dd($eTag->id);
            \array_push($tagIds, $eTag->id);
        }
        }


        
        $messageTagIds = array();
        foreach ($tagIds as $tag ) {
           $message_tag_id = DB::table('post_tags')->insertGetId([
                "post_id"=>$messageId,
                "tag_id"=>$tag
            ]);
            \array_push($messageTagIds, $message_tag_id);
        }

        // dd(["messgaeId"=>$messageId,"tags"=>$tagIds,"messageTags"=> $messageTagIds]);
        // $request->session()->flash("successMsg","Post Added Successfully");
        return redirect('/')->with('successMsg',"Post Added Successfully");
    }


    public function view($id)
    {
        $message = Posts::findOrFail($id);
        return view('post', [
            'message'=> $message
        ]);
        
    }

    public function delete(Request $request)
    {
        
        $id = $request->id;

        $message = Posts::find($id);

        $message->delete();
        return \redirect('/');
    }

    public function edit($id)
    {
        $message = Posts::find($id);

        return view('edit_post', ['message'=>$message]);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $message = Posts::find($id);

        $message->title = $request->title;
        $message->content = $request->content;

        $message->save();

        return \redirect('/')->with('successMsg','Post Successfully updated!');
    }
    
}
