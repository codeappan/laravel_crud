<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;
use \App\Posts;

class IndexController extends Controller
{
    public function index(Request $request){

        $errMsg = $request->session()->get('errMsg');
        $successMsg = $request->session()->get("successMsg");
        $messages = Posts::all();


        $messageArray = $messages->toArray();

        // dd($messageArray);
        // array_map(function($messageArray){

        //     // SELECT mt.id, mt.message_id, mt.tag_id, t.name FROM message_tags mt
        //     // JOIN tags t ON t.id = mt.tag_id 
        //     // WHERE message_id = 1
        //     dump($messageArray["id"]);

        //     $tags = DB::table('message_tags AS mt')
        //         ->join('tags AS t', 't.id', '=', 'mt.tag_id')
        //         ->where('mt.message_id', '=', $messageArray["id"])
        //         // ->pluck('symbol')->all();
        //         ->get();
        //      dump($tags->toArray());
        //     $messageArray["tags"] = $tags->toArray();
        // }, $messageArray);

        
        
        foreach ($messageArray as $key=>$message ) {
            $tags = DB::table('post_tags AS pt')
                ->join('tags AS t', 't.id', '=', 'pt.tag_id')
                ->where('pt.post_id', '=', $message["id"])
                // ->pluck('symbol')->all();
                ->get();
            //  dump($tags->toArray());
            $messageArray[$key]["tags"] = $tags->toArray();
            // dump($messageArray[$key]['tags']);
        }

        // dd($messageArray);

        // $messagesWithTags = DB::table('message_tags AS mt')
        //     ->join('messages AS m', 'mt.message_id', '=', 'm.id')
        //     ->join('tags AS t', 'mt.tag_id', '=', 't.id')
        //     ->select('m.*','t.name AS tag','t.id AS tag_id')
        //     ->get();

            // $uniqMsgs = array();

            // foreach ($messagesWithTags->items as $message) {
            //     foreach ($uniqMsgs as $msg) {
            //         if($msg->id == $message->id){
            //             array_push($msg->tags, $message->tag); 
            //         }else{
            //            array_push($uniqMsgs, $messagesWithTags);
            //            $uniqMsgs[sizeof($uniqMsgs) - 1]->tags = array();
            //            array_push($uniqMsgs[sizeof($uniqMsgs) - 1]->tags, $message->tag);
            //         }
            //     }
            // }
            // dd($uniqMsgs);

            // \array_map(function(){

            // }, $messagesWithTags->items);
        // dd($messagesWithTags);
        

        // array_map(function(){}, $messagesWithTags);
        
                // dd( $messageArray);

                // dd($messageArray);
                // dd($successMsg);
        return view('home', [
            'messages'=> $messageArray,
            'error'=>$errMsg,
            'success'=>$successMsg
        ]);
    }
}
